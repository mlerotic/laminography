'''
Created on Oct 02, 2015

@author: Mirna Lerotic
2nd Look Consulting
http://2ndlookconsulting.com
'''

import os
import numpy as np
import scipy as sp
import pylab as plt
from scipy.fftpack import fftshift, ifftshift, fft, ifft, fft2, ifft2
from scipy import signal
from scipy import interpolate

import Mrc

#----------------------------------------------------------------------
def write_mrc(stack, mrcfn):
    
    stackf32 = stack.astype(np.float32)
    Mrc.save(stackf32, mrcfn,ifExists='overwrite',calcMMM=False)
    
#----------------------------------------------------------------------
def read_mrc(mrcfn):
    
    stack = Mrc.load( mrcfn)
    
    return stack

#----------------------------------------------------------------------
def FBP(mu, theta, tiltangle, bpfilter, tau=1.0, interpolation = 'cubic'):
    
    
    nu = mu.shape[0]
    nv = mu.shape[1]
    M = mu.shape[2]
    
    print 'Data dims: nu = {0}, nv = {1}, M = {2}'.format(nu,nv,M)
    
    #output_size = 2 * np.floor(nu / (2 * np.sqrt(2)))
    output_size = float(max(nu,nv))
    output_sizez = float(max(nu,nv))
    
    print 'Output dims:{0}, {1}, {2}'.format(output_size, output_size, output_sizez)
    
    
    reconstructed = np.zeros((output_size,output_size,output_sizez))
    

    # resize image to next power of two for fourier analysis
    # speeds up fourier and lessens artifacts
    order = max(64, 2 ** np.ceil(np.log(2 * max(nu,nv)) / np.log(2)))
        
    #Create filter
    mid_indexu = np.ceil(nu/2)
    mid_indexv = np.ceil(nv/2)
    x = int(nu/2)*2+2
    y = int(nv/2)*2+2
    #[X, Y] = np.mgrid[0.0:x, 0.0:y]
    X = np.mgrid[0.0:x]
    Y = np.mgrid[0.0:y]
    xpr = X - (x) / 2.0
    ypr = Y - (y) / 2.0    
    
    
    
    if bpfilter == 1:
        ht = np.zeros((nu,nv))        
        
        for i in range(nu):
            for j in range(nv):
                if (i == 0) and (j==0):
                    ht[i,j] = np.sin(tiltangle)/(8*tau)
                elif (i % 2 != 0) and (j == 0):
                    ht[i,j] = - np.sin(tiltangle)/(2*np.pi**2*i**2*tau)
                else:
                    ht[i,j] = 0.0
     
        # zero pad input image
        htzero = np.zeros((order,order))
        htzero[:nu, :nv] = ht
        f = fft2(htzero)
        
    
    elif bpfilter == 2:
        #ramp filter
        f = fftshift(abs(np.mgrid[-1:1:2 / order])).reshape(-1, 1)
        
    elif bpfilter == 3:
        #Shepp-Logan filter
        f = fftshift(abs(np.mgrid[-1:1:2 / order])).reshape(-1, 1)
             
        w = 2 * np.pi * f
        f[1:] = f[1:] * np.sin(w[1:] / 2) / (w[1:] / 2)
        
                
    for itheta in range(M):
        print 'Projection {0} of {1}'.format(itheta+1,M)
        pt = mu[:,:,itheta]
        
        #Apply filtering
        ptzero = np.zeros((order,order))
        ptzero[:nu, :nv] = pt
        
        ptf = fft2(ptzero)        

        if bpfilter == 0:
            filtered = np.real(ifft2(ptf))
        else:
            filtered = np.real(ifft2(ptf*f))
        
        
#         plt.plot(abs(np.mgrid[-1:1:2 / order]))
#         plt.show()
#         
#         plt.plot(fftshift(abs(np.mgrid[-1:1:2 / order])).reshape(-1, 1))
#         plt.show()
                        
        
        qt = filtered[:nu, :nv]
        
        
        #backprojection
        x = output_size
        y = output_size
        z = output_sizez
        mid_index = np.ceil(x/2)
        mid_indez = np.ceil(z/2)
        [X, Y, Z] = np.mgrid[0.0:x, 0.0:y, 0.0:z]
        
        xpr = X - (output_size + 1.0) / 2.0
        ypr = Y - (output_size + 1.0) / 2.0
        zpr = Z - (output_sizez + 1.0) / 2.0
    
        
        #Rotation Ry x Rz - backprojection (works for the tube tiltangle = 0)
        #rx = xpr*np.cos(theta[itheta])*np.cos(tiltangle) + ypr*np.sin(theta[itheta])*np.cos(tiltangle) - zpr*np.sin(tiltangle)
        #ry = xpr*np.cos(theta[itheta])*np.sin(tiltangle) + ypr*np.sin(theta[itheta])*np.sin(tiltangle) + zpr*np.cos(tiltangle)
        
        #Backprojection Rotation Rz(-theta) x Ry(-fi)
        #rx = xpr*np.cos(theta[itheta])*np.cos(tiltangle) + ypr*np.sin(theta[itheta]) - zpr*np.cos(theta[itheta])*np.sin(tiltangle)
        #ry = xpr*np.sin(tiltangle) + zpr*np.cos(tiltangle)
        
        #Geometry from the paper
        rx = xpr*np.cos(theta[itheta]) + ypr*np.sin(theta[itheta]) 
        ry = xpr*np.cos(tiltangle)*np.sin(theta[itheta]) - ypr*np.cos(tiltangle)*np.cos(theta[itheta]) + zpr*np.sin(tiltangle)        
        
        if interpolation == 'nearest_neighbor':
            #Nearest neighbor
            k = np.round(mid_index + rx)
            l = np.round(mid_indez + ry)        
            reconstructed += qt[
                    ((((k > 0) & (k < nu)) * k) ).astype(np.int), 
                    ((((l > 0) & (l < nv)) * l) ).astype(np.int)]
        
        elif interpolation == 'cubic':
            #Cubic interpolation
            rx += mid_index
            ry += mid_indez
            
            [gX, gY] = np.mgrid[0.0:qt.shape[0], 0.0:qt.shape[1]]
                   
            
            intp = interpolate.griddata((gX.ravel(), gY.ravel()), qt.ravel(),
                              ((rx, ry)), method='cubic', fill_value=0.0)
            reconstructed += intp


    reconstructed = reconstructed * np.pi / (2 * M)
        
    return reconstructed


#----------------------------------------------------------------------                        
def main():
    
    print 'Reading settings from the config file'
    
    
    SettingsFileName = 'laminographypy_config.txt'
    
    bpfilter = 0
    tau = 1.0
    #Tilt angle 
    tiltangle = 0.0
    
    
    try:
    #if True:
        f = open(SettingsFileName, 'rt')
        for line in f:
                    
            if 'DIRECTORY' in line : 
                slist = line.split('=')
                slist = ''.join(slist[1:])
                value = slist.strip()
                slist = value.split('\\')
                value = os.path.join(slist[0],os.sep,*slist[1:])
                #os.path.join(os.sep, 'usr', 'lib')
                data_folder = value

            if 'DATA_FILENAME' in line : 
                slist = line.split('=')
                value = slist[1].strip()
                datafilename = value
                
            if 'ANGLES_FILENAME' in line : 
                slist = line.split('=')
                value = slist[1].strip()
                anglesfilename = value
                
            if 'SAVE_FILENAME' in line : 
                slist = line.split('=')
                value = slist[1].strip()
                savefilename = value
                
            if 'TILT_ANGLE' in line : 
                slist = line.split('=')
                value = slist[1].strip()
                tiltangle = float(value)                
                    
            if 'FILTER' in line : 
                slist = line.split('=')
                value = slist[1].strip()
                bpfilter = int(value)
                
            if 'TAU' in line : 
                slist = line.split('=')
                value = slist[1].strip()
                tau = float(value)    
                                    
        f.close()
        
    except:
        print 'Could not read settings file. Exiting.'
        return
    

    #Read the data
    filepath = os.path.join(data_folder, datafilename)
    print 'Data file', filepath
    stack = read_mrc(filepath)
    
    stack = np.transpose(stack, (1,2,0))
    
    #Read in the angles
    anglefilepath = os.path.join(data_folder, anglesfilename)    

    
    print 'Data dims:', stack.shape
    
    f = open(anglefilepath,'r')
     
    tlist = []   
 
    for line in f:
        if line.startswith("*"):
            pass
        else:
            t = line 
            if t.strip() == '':
                continue
            tlist.append(float(t))
                

    theta = np.array(tlist)
             
    f.close()    
    
    print 'Angles:', theta
    print 'N angles:', len(theta)    
    print 'Tilt angle:', tiltangle
    
    if bpfilter == 0:
        print 'No filter'
    elif bpfilter == 1:
        print 'Laminography filter'
        print 'Detector pitch tau={0}'.format(tau)
    elif bpfilter == 2:
        print 'Ramp filter'
    elif bpfilter == 3:
        print 'Shepp-logan filter'
    else:
        print 'Filter choice not recognized. Allowed filter choices are are 0,1,2 or 3:\n\t0-no filter\n\t1-laminography filter\n\t2-ramp filter\n\t3-shepp-logan filter'
        return
    
    
    
    #Convert the angles to radians
    tiltangle = np.deg2rad(tiltangle)
    theta = np.deg2rad(theta)

    reconstructed = FBP(stack, theta, tiltangle, bpfilter, tau=tau)
 
     
    #Save the 3D reconstruction to a MRC file
    savefilepath = os.path.join(data_folder, savefilename)
    write_mrc(reconstructed, savefilepath)    
    
    print 'Finished. Bye...'
            

if __name__ == '__main__':
    main()
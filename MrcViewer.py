'''
Created on Nov 7, 2016

@author: Mirna Lerotic
'''

# 
#   This file is part of Mantis, a Multivariate ANalysis Tool for Spectromicroscopy.
# 
#   Copyright (C) 2013 Mirna Lerotic, 2nd Look
#   http://2ndlookconsulting.com
#   License: GNU GPL v3
#
#   Mantis is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   any later version.
#
#   Mantis is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details <http://www.gnu.org/licenses/>.


import sys
import os
import numpy as np
import time
import getopt

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import Qt, QCoreApplication

from PIL import Image  



import matplotlib 
from numpy import NAN
matplotlib.rcParams['backend.qt4'] = 'PyQt4'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from mpl_toolkits.axes_grid import make_axes_locatable
from matplotlib.widgets import LassoSelector
matplotlib.interactive( True )
matplotlib.rcParams['svg.fonttype'] = 'none'

import Mrc

#----------------------------------------------------------------------
def write_mrc(stack, mrcfn):
    
    stackf32 = stack.astype(np.float32)
    Mrc.save(stackf32, mrcfn,ifExists='overwrite',calcMMM=False)
    
#----------------------------------------------------------------------
def read_mrc(mrcfn):
    
    stack = Mrc.load( mrcfn)
    
    return stack

version = '2.3.03'

if sys.platform == 'win32':
    Winsizex = 1000
    Winsizey = 800
else:
    Winsizex = 1250
    Winsizey = 900

PlotH = 4.0
PlotW = PlotH*1.61803

ImgDpi = 40

verbose = False

showtomotab = 1


        
        
""" ------------------------------------------------------------------------------------------------"""
class PageMrc(QtGui.QWidget):
    def __init__(self):
        super(PageMrc, self).__init__()

        self.initUI()
        
#----------------------------------------------------------------------          
    def initUI(self): 

        

        #panel 1
        sizer1 = QtGui.QGroupBox('Tomo Data')
        vbox1 = QtGui.QVBoxLayout()

        
        self.button_loadmrc = QtGui.QPushButton('Load 3D Dataset (.mrc)')
        self.button_loadmrc.clicked.connect( self.OnLoadSingleMrc)
        vbox1.addWidget(self.button_loadmrc)        

        sizer1.setLayout(vbox1)


                
 
        #panel 5    
         
        vbox5 = QtGui.QVBoxLayout()
        vbox5.addStretch(1)

        gridsizer5 = QtGui.QGridLayout()
        gridsizer5.setSpacing(5)
        
        self.tc_1 = QtGui.QLabel(self)
        self.tc_1.setText("XY Slice: ")
        gridsizer5.addWidget(self.tc_1, 0, 1, QtCore .Qt. AlignLeft)
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.absimgfig = Figure((PlotH*0.9, PlotH*0.9))

        self.AbsImagePanel = FigureCanvas(self.absimgfig)
        self.AbsImagePanel.setParent(self)
        
        
        fbox.addWidget(self.AbsImagePanel)
        frame.setLayout(fbox)
        gridsizer5.addWidget(frame, 1, 1, QtCore .Qt. AlignLeft)
        

        self.slider_slice1 = QtGui.QScrollBar(QtCore.Qt.Vertical)
        self.slider_slice1.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_slice1.valueChanged[int].connect(self.OnScrollSlice1)
        self.slider_slice1.setRange(0, 100)
        self.slider_slice1.setEnabled(False)
        
        gridsizer5.addWidget(self.slider_slice1, 1, 0, QtCore .Qt. AlignLeft)
        
        
        self.tc_2 = QtGui.QLabel(self)
        self.tc_2.setText("XZ Slice: ")
        gridsizer5.addWidget(self.tc_2, 2, 1, QtCore .Qt. AlignLeft)
        
        frame2 = QtGui.QFrame()
        frame2.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox2 = QtGui.QHBoxLayout()
   
        self.absimgfig2 = Figure((PlotH*0.9, PlotH*0.9))

        self.AbsImagePanel2 = FigureCanvas(self.absimgfig2)
        self.AbsImagePanel2.setParent(self)
        
        
        fbox2.addWidget(self.AbsImagePanel2)
        frame2.setLayout(fbox2)
        
 
        gridsizer5.addWidget(frame2, 3, 1, QtCore .Qt. AlignLeft)    
        
        self.slider_slice2 = QtGui.QScrollBar(QtCore.Qt.Vertical)
        self.slider_slice2.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_slice2.valueChanged[int].connect(self.OnScrollSlice2)
        self.slider_slice2.setRange(0, 100)
        self.slider_slice2.setEnabled(False)
        
        gridsizer5.addWidget(self.slider_slice2, 3, 0, QtCore .Qt. AlignLeft)    
        
        self.tc_3 = QtGui.QLabel(self)
        self.tc_3.setText("YZ Slice: ")
        gridsizer5.addWidget(self.tc_3, 2, 2, QtCore .Qt. AlignLeft)
        
        
        frame3 = QtGui.QFrame()
        frame3.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox3 = QtGui.QHBoxLayout()
   
        self.absimgfig3 = Figure((PlotH*0.9, PlotH*0.9))

        self.AbsImagePanel3 = FigureCanvas(self.absimgfig3)
        self.AbsImagePanel3.setParent(self)
        
        
        fbox3.addWidget(self.AbsImagePanel3)
        frame3.setLayout(fbox3)
        gridsizer5.addWidget(frame3, 3, 2, QtCore .Qt. AlignLeft)     
        
        
        self.slider_slice3 = QtGui.QScrollBar(QtCore.Qt.Vertical)
        self.slider_slice3.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_slice3.valueChanged[int].connect(self.OnScrollSlice3)
        self.slider_slice3.setRange(0, 100)
        self.slider_slice3.setEnabled(False)
        
        gridsizer5.addWidget(self.slider_slice3, 3, 3, QtCore .Qt. AlignLeft)        

        vbox5.addLayout(gridsizer5)
        vbox5.addStretch(1)
        
       
        vboxtop = QtGui.QVBoxLayout()
        
        hboxtop = QtGui.QHBoxLayout()
        vboxt1 = QtGui.QVBoxLayout()
        vboxt1.addWidget(sizer1)
        vboxt1.addStretch (1)

        
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vboxt1)
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vbox5)
        hboxtop.addStretch (0.5)
        



        vboxtop.addStretch (0.5)
        vboxtop.addLayout(hboxtop)
        vboxtop.addStretch (0.9)

        vboxtop.setContentsMargins(20,20,20,20)
        self.setLayout(vboxtop)
        
        
        

#----------------------------------------------------------------------          
    def OnLoadSingleMrc(self, event):
                        

        wildcard = "Supported 4D formats (*.mrc *.ali);;Mrc files (*.mrc *.ali);;"


        OpenFileName = QtGui.QFileDialog.getOpenFileName(self, 'Load Tomo Dataset', '', wildcard,
                                                         None, QtGui.QFileDialog.DontUseNativeDialog)

        OpenFileName = str(OpenFileName)
        if OpenFileName == '':
            return
        
        basename, extension = os.path.splitext(OpenFileName) 
        
        if extension in ['.mrc', '.ali']:
            
            self.stack = read_mrc(OpenFileName)
        
        dims = self.stack.shape    
        print 'Data dimenstions: ',dims
        self.islice1 = int(dims[2]/2)
        self.islice2 = int(dims[1]/2)
        self.islice3 = int(dims[0]/2)
        
        self.slider_slice1.setValue(self.islice1)
        self.slider_slice1.setRange(0, dims[2]-1)
        self.slider_slice1.setEnabled(True)
        
       
        self.slider_slice2.setValue(self.islice2)
        self.slider_slice2.setRange(0, dims[1]-1)
        self.slider_slice2.setEnabled(True)
        
        
        self.slider_slice3.setValue(self.islice3)
        self.slider_slice3.setRange(0, dims[0]-1)
        self.slider_slice3.setEnabled(True)
                
               
#----------------------------------------------------------------------            
    def OnScrollSlice1(self, value):
        self.islice1 = value

        self.ShowImage()
        
#----------------------------------------------------------------------            
    def OnScrollSlice2(self, value):
        self.islice2 = value

        self.ShowImage()
        
#----------------------------------------------------------------------            
    def OnScrollSlice3(self, value):
        self.islice3 = value

        self.ShowImage()

#----------------------------------------------------------------------        
    def ShowImage(self):
        

        
        image = self.stack[:,:,self.islice1]

        fig = self.absimgfig
        fig.clf()
        fig.add_axes(((0.0,0.0,1.0,1.0)))
        axes = fig.gca()
        fig.patch.set_alpha(1.0)
         
        im = axes.imshow(image, cmap=matplotlib.cm.get_cmap("gray")) 
    
             
        
        axes.axis("off")      
        self.AbsImagePanel.draw()
        self.axes = axes
                
        
        #Show orthogonal slices
        dims = self.stack.shape
        image2 = self.stack[:,self.islice2,:]

        fig = self.absimgfig2
        fig.clf()
        fig.add_axes(((0.0,0.0,1.0,1.0)))
        axes2 = fig.gca()
        fig.patch.set_alpha(1.0)
         
        im = axes2.imshow(image2, cmap=matplotlib.cm.get_cmap("gray"))         

        axes2.axis("off")      
        self.AbsImagePanel2.draw()


        image3 = self.stack[self.islice3,:,:]

        fig = self.absimgfig3
        fig.clf()
        fig.add_axes(((0.0,0.0,1.0,1.0)))
        axes3 = fig.gca()
        fig.patch.set_alpha(1.0)
         
        im = axes3.imshow(image3, cmap=matplotlib.cm.get_cmap("gray"))         

        axes3.axis("off")      
        self.AbsImagePanel3.draw()         
        
        self.tc_1.setText("XY Slice: {0}".format(self.islice1+1))
        self.tc_2.setText("XZ Slice: {0}".format(self.islice2+1))
        self.tc_3.setText("YZ Slice: {0}".format(self.islice3+1))
        


        
""" ------------------------------------------------------------------------------------------------"""
class MainFrame(QtGui.QMainWindow):
    
    def __init__(self):
        super(MainFrame, self).__init__()
        
        self.initUI()
        


#----------------------------------------------------------------------          
    def initUI(self):   
        

               

        self.resize(Winsizex, Winsizey)
        self.setWindowTitle('3D Viewer')
        

                            
        
        tabs = QtGui.QTabWidget()
        


        

        if showtomotab:
            self.page8 = PageMrc()
            tabs.addTab(self.page8, "Mrc")  
            
        if sys.platform == 'win32':
            tabs.setMinimumHeight(750)
        else:
            tabs.setMinimumHeight(850)
                    

        if showtomotab:
            tabs.tabBar().setTabTextColor(8, QtGui.QColor('darkblue')) 

       
        
        
        # Only add "expert" pages if option "--key" is given in command line
        try:
            options, extraParams = getopt.getopt(sys.argv[1:], '', ['wx', 'batch', 'nnma', 'ica', 'keyeng'])
        except:
            print 'Error - wrong command line option used. Available options are --wx, --batch and --nnma'
            return
        
#         for opt, arg in options:        
#             if opt in '--nnma':
#                 if verbose: print "Running with NNMA."
#                 self.page7 = PageNNMA(self.common, self.data_struct, self.stk, self.anlz, self.nnma)
#                 tabs.addTab(self.page7, "NNMA Analysis")

        


    
        layout = QVBoxLayout()

        layout.addWidget(tabs)
        #self.setCentralWidget(tabs)
        
        self.scrollArea = QtGui.QScrollArea()
        #self.scrollArea.setBackgroundRole(QtGui.QPalette.Dark)
        self.scrollArea.setWidget(tabs)
        #self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        #self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea.setWidgetResizable(True)
        self.setCentralWidget(self.scrollArea)
        
        
        screen = QtGui.QDesktopWidget().screenGeometry()
        
        
        if screen.height() < Winsizey - 50:
            self.showMaximized()
       
                              
        self.show()
        if sys.platform == "darwin":
            self.raise_()

        
        
        
                
""" ------------------------------------------------------------------------------------------------"""
                        
def main():
    
    
    app = QtGui.QApplication(sys.argv)
    frame = MainFrame()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
